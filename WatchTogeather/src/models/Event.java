package models;

import java.sql.Timestamp;

public class Event {
    private long id;
    private String name;
    private Timestamp date;
    private String place;
    private String imageUrl;

    public Event(long id, String name, Timestamp date, String place, String imageUrl) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.place = place;
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Event(long id, String name, Timestamp date, String place) {
        this.id = id;
        this.name = name;
        this.date = date;

        this.place = place;

    }

    public Event(String name, Timestamp date, String place) {
        this.name = name;
        this.date = date;

        this.place = place;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }


    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

}
