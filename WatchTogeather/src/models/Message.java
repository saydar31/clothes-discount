package models;

import java.sql.Timestamp;

public class Message {
    private User sender;
    private User recipient;
    private String value;
    private Timestamp time;
    public Message(User sender, User recipient, String value, Timestamp time) {
        this.sender = sender;
        this.recipient = recipient;
        this.value = value;
        this.time = time;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }


}
