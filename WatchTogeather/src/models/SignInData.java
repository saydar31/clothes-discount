package models;

public class SignInData {
    private String login;
    private String password;
    private long id;

    public SignInData(String login, String password, long id) {
        this.login = login;
        this.password = password;
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public long getId() {
        return id;
    }

    public SignInData(String password, long id) {
        this.password = password;
        this.id = id;
    }
}
