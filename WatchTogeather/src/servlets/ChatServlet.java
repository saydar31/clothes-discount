package servlets;

import dao.MessagesRepository;
import dao.UserCrudImpl;
import models.Message;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@WebServlet("/chat")
public class ChatServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        if (user != null) {
            Long interlocutorId = Long.parseLong(req.getParameter("id"));
            User interlocutor = UserCrudImpl.getInstance().find(interlocutorId).get();
            List<Message> dialog = MessagesRepository.getInstance().getDialog(user, interlocutor);
            String interlocutorName = interlocutor.getFirstName() + " " + interlocutor.getLastName();
            req.setAttribute("messages", dialog);
            req.setAttribute("interlocutorName", interlocutorName);
            req.setAttribute("recipientId", interlocutorId);
            getServletContext().getRequestDispatcher("/chat.jsp").forward(req, resp);
        } else {
            resp.sendRedirect("/WatchTogeather_war_exploded/sign_in");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("user") != null) {
            Long senderId = Long.valueOf(req.getParameter("sender_id"));
            Long recipientId = Long.valueOf(req.getParameter("recipient_id"));
            String value = req.getParameter("message");
            User sender = UserCrudImpl.getInstance().find(senderId).get();
            User recipient = UserCrudImpl.getInstance().find(recipientId).get();
            MessagesRepository.getInstance().saveMessage(new Message(sender, recipient, value, new Timestamp(System.currentTimeMillis())));
            resp.sendRedirect("/WatchTogeather_war_exploded/chat?id=" + recipientId);
        }
    }
}
