package servlets;

import dao.EventCrudImpl;
import models.Event;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@WebServlet("/event")
public class EventServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idString = req.getParameter("id");
        if (idString != null) {
            Long id = Long.parseLong(idString);
            Optional<Event> eventCondidate = EventCrudImpl.getInstance().find(id);
            if (eventCondidate.isEmpty()) {
                resp.setStatus(404);
                getServletContext().getRequestDispatcher("/404.jsp").forward(req, resp);
            } else {
                Event event = eventCondidate.get();
                List<User> eventVisitors = EventCrudImpl.getInstance().eventVisitors(id);
                req.setAttribute("event", event);
                req.setAttribute("eventVisitors", eventVisitors);
                req.setAttribute("inVisitors",eventVisitors.contains(req.getSession().getAttribute("user")));
                getServletContext().getRequestDispatcher("/event.jsp").forward(req, resp);
            }

        } else {

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long eventId = Long.valueOf(req.getParameter("event_id"));
        Event event = EventCrudImpl.getInstance().find(eventId).get();
        User user = (User) req.getSession().getAttribute("user");
        EventCrudImpl.getInstance().addVisitor(user, event);
        resp.sendRedirect("http://localhost:8080/WatchTogeather_war_exploded/event?id=" + eventId);
    }
}
