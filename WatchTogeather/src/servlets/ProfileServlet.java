package servlets;

import dao.UserCrudImpl;
import models.Event;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        User currentPageUser = user;
        List<Event> eventList;
        Long id = null;
        if (user != null) {
            if (req.getParameter("id") != null) {
                id = Long.parseLong(req.getParameter("id"));
                currentPageUser = UserCrudImpl.getInstance().find(id).get();
            }
            eventList = UserCrudImpl.getInstance().getEvents(currentPageUser);
            req.setAttribute("currentPageUser", currentPageUser);
            req.setAttribute("eventList", eventList);
            Boolean notMyPage = !(id == null || user.getId().equals(id));
            req.setAttribute("myPage", !notMyPage);
            req.setAttribute("notMyPage", notMyPage);
            req.setAttribute("infoIsPresent", currentPageUser.getInfo() != null);
            getServletContext().getRequestDispatcher("/profile.jsp").forward(req, resp);
        } else {
            resp.sendRedirect("/WatchTogeather_war_exploded/sign_in");
        }
    }
}
