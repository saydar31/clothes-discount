package servlets;

import dao.UserCrudImpl;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

//52 17
@WebServlet("/im")
public class ChatsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        if (user != null) {
            Set<User> interlocutors = UserCrudImpl.getInstance().getInterlocutors(user);
            req.setAttribute("chatmates", interlocutors);
            getServletContext().getRequestDispatcher("/chats.jsp").forward(req, resp);
        } else {
            resp.sendRedirect("/WatchTogeather_war_exploded/sign_in");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
