package servlets;

import dao.UserCrudImpl;
import models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/profile_settings")
@MultipartConfig(fileSizeThreshold = 6291456, // 6 MB
        maxFileSize = 10485760L, // 10 MB
        maxRequestSize = 20971520L // 20 MB
)
public class ProfileSettingsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        if (user != null) {
            getServletContext().getRequestDispatcher("/profileSettings.jsp").forward(req, resp);
        } else {
            resp.sendRedirect("/WatchTogeather_war_exploded/sign_in");
        }
    }

    final String UPLOAD_DIR = "images/user";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        Long id = user.getId();
        String newFirstName = req.getParameter("newFirstName");
        String newLastName = req.getParameter("newLastName");
        String newInfo = req.getParameter("info");
        User updatedUser = new User(id, newFirstName, newLastName, newInfo);
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        String newAvatarUrl = "";
        String applicationPath = req.getServletContext().getRealPath("");
        String uploadFolder = applicationPath + UPLOAD_DIR;
        for (Part part : req.getParts()) {
            if (part != null && part.getSize() > 0) {
                String fileName = part.getSubmittedFileName();
                String contentType = part.getContentType();
                // пропустим все некартинки :)
                if (contentType == null || !contentType.equalsIgnoreCase("image/jpeg")) {
                    continue;
                }
                String newAvatarPath = uploadFolder + File.separator + fileName;
                newAvatarUrl = "images/user/" + fileName;
                File file = new File(newAvatarPath);
                file.createNewFile();
                part.write(newAvatarPath);
            }
        }
        updatedUser.setAvatarUrl(newAvatarUrl);
        UserCrudImpl.getInstance().update(updatedUser);
        req.getSession().setAttribute("user", updatedUser);
        resp.sendRedirect("/WatchTogeather_war_exploded/profile");
    }
}
