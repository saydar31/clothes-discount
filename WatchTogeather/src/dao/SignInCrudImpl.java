package dao;

import models.SignInData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class SignInCrudImpl implements Crud<SignInData> {
    private static SignInCrudImpl instance;

    private Connection connection;

    public static SignInCrudImpl getInstance() {
        return instance;
    }

    static {
        instance = new SignInCrudImpl();
    }

    private SignInCrudImpl() {
        connection = ConnectionKeeperObject.getInstance().getConnection();
    }

    //language=SQL
    private final String CREATE_SIGN_IN_DATA_SQL = "insert into sign_in values (?,?,?);";

    @Override
    public void create(SignInData signInData) {
        try {
            PreparedStatement statement = connection.prepareStatement(CREATE_SIGN_IN_DATA_SQL);
            statement.setString(1, signInData.getLogin());
            statement.setString(2, signInData.getPassword());
            statement.setLong(3, signInData.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException();
        }
    }

    //language=SQL
    private final String FIND_SQL = "select * from sign_in where user_id=?";

    @Override
    public Optional<SignInData> find(long id) {
        return null;
    }

    @Override
    public void delete(long id) {

    }

    @Override
    public void update(SignInData signInData) {

    }

    private RowMapper<Long> idRowMapper = row -> row.getLong("user_id");
    //language=SQL
    private final String IDENTIFY_SQL = "select user_id from sign_in where login=? and password=?";

    public Long id(String login, String password) {
        try {
            PreparedStatement statement = connection.prepareStatement(IDENTIFY_SQL);
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                return idRowMapper.mapRow(set);
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException();
        }
    }

    //language=SQL
    private final String EXIST_SQL = "select user_id from sign_in where login=?";

    public boolean exist(String login) {
        try {
            PreparedStatement statement = connection.prepareStatement(EXIST_SQL);
            statement.setString(1, login);
            ResultSet set = statement.executeQuery();
            return set.next();
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
    }
}
