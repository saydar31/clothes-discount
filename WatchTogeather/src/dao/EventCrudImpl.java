package dao;

import models.Event;
import models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EventCrudImpl implements Crud<Event> {
    private Connection connection;

    private static EventCrudImpl instance;

    static {
        instance = new EventCrudImpl();
        instance.connection = ConnectionKeeperObject.getInstance().getConnection();
    }

    private EventCrudImpl() {

    }

    public static EventCrudImpl getInstance() {
        return instance;
    }

    @Override
    public void create(Event event) {

    }

    @Override
    public void update(Event event) {

    }


    RowMapper<Event> eventRowMapper = row -> {
        Long id = row.getLong("id");
        String name = row.getString("name");
        String place = row.getString("place");
        Timestamp eventTime = row.getTimestamp("event_time");
        Event event = new Event(id, name, eventTime, place);
        return event;
    };

    @Override
    public Optional<Event> find(long id) {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement("select * from event where id=?");
            statement.setLong(1, id);
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                Event event = eventRowMapper.mapRow(set);
                return Optional.ofNullable(event);
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException();
        }
    }

    @Override
    public void delete(long id) {

    }

    //language=SQL
    private final String EVENTS_SQL = "select * from event order by event_time desc;";

    public List<Event> getEvents() {
        List<Event> result = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(EVENTS_SQL);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                result.add(eventRowMapper.mapRow(set));
            }
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
        return result;
    }

    public void addVisitor(User visitor, Event event) {
        try {
            PreparedStatement statement = connection.prepareStatement("insert into visitor (user_id, event_id) VALUES (?,?);");
            statement.setLong(1, visitor.getId());
            statement.setLong(2, event.getId());
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
    }

    public List<User> eventVisitors(Long eventId) {
        List<User> eventVisitors = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("select user_id from visitor where event_id=?");
            statement.setLong(1, eventId);
            ResultSet set = statement.executeQuery();
            RowMapper<Long> userIdRowMapper = row -> row.getLong("user_id");
            while (set.next()) {
                Long userId = userIdRowMapper.mapRow(set);
                eventVisitors.add(UserCrudImpl.getInstance().find(userId).get());
            }
            return eventVisitors;
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
    }
}
