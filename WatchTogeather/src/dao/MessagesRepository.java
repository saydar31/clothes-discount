package dao;

import models.Message;
import models.User;

import java.sql.*;
import java.util.*;

public class MessagesRepository {
    private static MessagesRepository instance;
    private Connection connection;

    private MessagesRepository() {
        connection = ConnectionKeeperObject.getInstance().getConnection();
    }

    public static MessagesRepository getInstance() {
        return instance;
    }

    static {
        instance = new MessagesRepository();
    }

    public void saveMessage(Message message) {
        try {
            PreparedStatement statement = connection.prepareStatement("insert into wtmessage (sender_id, recipient_id, value, time) VALUES (?,?,?,?)");
            statement.setLong(1, message.getSender().getId());
            statement.setLong(2, message.getRecipient().getId());
            statement.setString(3, message.getValue());
            statement.setTimestamp(4, message.getTime());
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
    }

    RowMapper<Message> messageRowMapper = row -> {
        UserCrudImpl userRep = UserCrudImpl.getInstance();
        User sender = userRep.find(row.getLong("sender_id")).get();
        User recipient = userRep.find(row.getLong("recipient_id")).get();
        String value = row.getString("value");
        Timestamp timestamp = row.getTimestamp("time");
        return new Message(sender, recipient, value, timestamp);
    };

    public Message getChatLastMessage(User user, User user2) {
        try {
            PreparedStatement statement = connection.prepareStatement("select * from wtmessage where time = (select max(time) from wtmessage where (sender_id=? and recipient_id=?)or (sender_id=? and recipient_id=?))");
            statement.setLong(1, user.getId());
            statement.setLong(2, user2.getId());
            statement.setLong(3, user2.getId());
            statement.setLong(4, user.getId());
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                return messageRowMapper.mapRow(set);
            } else {
                throw new SQLException();
            }
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
    }

    public List<Message> getDialog(User user, User user2) {
        List<Message> dialog = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("select * from wtmessage where (sender_id=? and recipient_id=?)or (sender_id=? and recipient_id=?) order by time");
            statement.setLong(1, user.getId());
            statement.setLong(2, user2.getId());
            statement.setLong(3, user2.getId());
            statement.setLong(4, user.getId());
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                dialog.add(messageRowMapper.mapRow(set));
            }
            return dialog;
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
    }

    public List<User> getInterlocutors(User user) {
        List<User> interlocutors = new ArrayList<>();
        try {
            Set<Long> userSet = new HashSet<>();
            PreparedStatement statement = connection.prepareStatement("select sender_id from wtmessage where recipient_id=?");
            statement.setLong(1, user.getId());
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                userSet.add(set.getLong("sender_id"));
            }
            statement = connection.prepareStatement("select recipient_id from wtmessage where sender_id=?");
            statement.setLong(1, user.getId());
            set = statement.executeQuery();
            while (set.next()) {
                userSet.add(set.getLong("recipient_id"));
            }
            userSet.forEach(aLong -> interlocutors.add(UserCrudImpl.getInstance().find(aLong).get()));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException();
        }
        return interlocutors;
    }


    public List<Message> getLastMessages(User user) {
        List<Message> messages = new ArrayList<>();
        List<User> interlocutors = getInterlocutors(user);
        interlocutors.forEach(user1 -> messages.add(getChatLastMessage(user, user1)));
        messages.sort(Comparator.comparing(Message::getTime));
        return messages;
    }
}
