package dao;

import models.Event;
import models.User;

import java.sql.*;
import java.util.*;

public class UserCrudImpl implements Crud<User> {

    private static UserCrudImpl instance;
    private Connection connection;

    public static UserCrudImpl getInstance() {
        return instance;
    }


    private UserCrudImpl() {
        connection = ConnectionKeeperObject.getInstance().getConnection();
    }

    static {
        instance = new UserCrudImpl();
    }

    //language=SQL
    private final String CREATE_USER_SQL = "insert into wtuser (first_name, last_name) values(?,?) returning id;";

    private RowMapper<User> userRowMapper = row -> {
        Long id = row.getLong("id");
        String firstName = row.getString("first_name");
        String lastName = row.getString("last_name");
        String info = row.getString("info");
        String avatarUrl = row.getString("avatar_url");
        return new User(id, firstName, lastName, info, avatarUrl);
    };

    RowMapper<Long> idRowMapper = row -> row.getLong("id");

    @Override
    public void create(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement(CREATE_USER_SQL);
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            ResultSet set = statement.executeQuery();
            set.next();
            Long id = idRowMapper.mapRow(set);
            user.setId(id);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    //language=SQL
    private final String FIND_SQL = "select * from wtuser where id=?;";

    @Override
    public Optional<User> find(long id) {
        Optional<User> result = Optional.empty();
        try {
            PreparedStatement statement = connection.prepareStatement(FIND_SQL);
            statement.setLong(1, id);
            ResultSet set = statement.executeQuery();
            if (set.next()) {
                return Optional.ofNullable(userRowMapper.mapRow(set));
            }
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
        return result;
    }

    @Override
    public void delete(long id) {

    }

    @Override
    public void update(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement("update wtuser set (first_name,last_name,info,avatar_url)=(?,?,?,?)  where id = ?;");
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getInfo());
            statement.setString(4, user.getAvatarUrl());
            statement.setLong(5, user.getId());
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException();
        }
    }

    public List<Event> getEvents(User user) {
        List<Event> result = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement("select * from visitor where user_id=?");
            statement.setLong(1, user.getId());
            ResultSet set = statement.executeQuery();
            RowMapper<Long> userIdRowMapper = row -> row.getLong("event_id");
            while (set.next()) {
                Optional<Event> eventOptional = EventCrudImpl.getInstance().find(userIdRowMapper.mapRow(set));
                if (eventOptional.isPresent()) {
                    result.add(EventCrudImpl.getInstance().find(userIdRowMapper.mapRow(set)).get());
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException();
        }
        return result;
    }

    public Set<User> getInterlocutors(User user) {
        Set<User> interlocutors = new LinkedHashSet<>();
        try {
            PreparedStatement statement = connection.prepareStatement("select wtmessage.sender_id, wtmessage.recipient_id,time\n" +
                    "from wtmessage\n" +
                    "where recipient_id = ?\n" +
                    "   or sender_id = ?\n" +
                    "order by time desc ;");
            statement.setLong(1, user.getId());
            statement.setLong(2, user.getId());
            ResultSet set = statement.executeQuery();
            LinkedHashSet<Long> interlocutorsIdSet = new LinkedHashSet<>();
            while (set.next()) {
                Long senderId = set.getLong("sender_id");
                long recipientId = set.getLong("recipient_id");
                interlocutorsIdSet.add(senderId.equals(user.getId()) ? recipientId : senderId);
            }
            interlocutorsIdSet.forEach(id -> interlocutors.add(UserCrudImpl.getInstance().find(id).get()));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException();
        }
        return interlocutors;
    }

    //language=SQL
//    private final String FIND_BY_NAME_SQL = "select id from wtuser where first_name=? and last_name=?";
//
//    public Long findByName(String firstName, String lastName) {
//        try {
//            PreparedStatement statement = connection.prepareStatement(FIND_BY_NAME_SQL);
//            statement.setString(1, firstName);
//            statement.setString(2, lastName);
//            ResultSet set = statement.executeQuery();
//            if (set.next()) {
//                return idRowMapper.mapRow(set);
//            } else {
//                throw new SQLException();
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//            throw new IllegalStateException();
//        }
//    }
}
