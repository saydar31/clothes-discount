<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aydar
  Date: 20.10.2019
  Time: 19:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Watch Together: Sign in</title>
</head>

<body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>

<style>
    input[type="button"]:not(.default),
    input[type="submit"]:not(.default) {
        -webkit-border-radius: 3px;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 3px;
        -moz-background-clip: padding;
        border-radius: 3px;
        background-clip: padding-box;
        -webkit-transition: color 0.2s ease, border 0.2s ease, background 0.2s ease, -webkit-box-shadow 0.2s ease;
        -moz-transition: color 0.2s ease, border 0.2s ease, background 0.2s ease, -moz-box-shadow 0.2s ease;
        -o-transition: color 0.2s ease, border 0.2s ease, background 0.2s ease, box-shadow 0.2s ease;
        transition: color 0.2s ease, border 0.2s ease, background 0.2s ease, box-shadow 0.2s ease;
        position: relative;
        margin: 0 7px;
        display: inline-block;
        min-width: 144px;
        max-width: 100%;
        padding: 15px 25px;
        font-family: "Arial", "Helvetica Neue", Arial, Helvetica, sans-serif;
        font-size: 14px;
        font-weight: 600;
        text-transform: uppercase;
        line-height: 1;
        border-width: 1px;
        border-style: solid;
        background-color: steelblue;
        color: #ddd;
    }

    .form {
        zoom: 1;
        display: block;
        width: auto;
        padding: 25px 0 0;
    }

    .form:before,
    .form:after {
        content: "";
        display: table;
    }

    .form:after {
        clear: both;
    }

    .form .form-group {
        zoom: 1;
        position: relative;
        margin-bottom: 25px;
    }

    .form .form-group:before,
    .form .form-group:after {
        content: "";
        display: table;
    }

    .form .form-group:after {
        clear: both;
    }

    .form .form-group:after {
        content: '';
        display: block;
        clear: both;
    }

    .form .form-group[class*="col-"] input[type=text],
    .form .form-group[class*="col-"] input[type=email],
    .form .form-group[class*="col-"] input[type=password] {
        display: inline-block;
        width: 100%;
        min-width: 0;
        max-width: 100%;
    }

    .form .form-control {
        -webkit-border-radius: 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0;
        -moz-background-clip: padding;
        border-radius: 0;
        background-clip: padding-box;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        height: auto;
        font-size: 17px;
        font-weight: 300;
        background-color: #ffffff;
        border-style: solid;
        border-width: 1px 1px 1px 3px;
        border-top-color: #d7d7d7;
        border-right-color: #d7d7d7;
        border-bottom-color: #d7d7d7;
    }

    input[type=text],
    input[type=email],
    input[type=password] {
        -webkit-transition: color 0.2s ease, background 0.3s ease;
        -moz-transition: color 0.2s ease, background 0.3s ease;
        -o-transition: color 0.2s ease, background 0.3s ease;
        transition: color 0.2s ease, background 0.3s ease;
        -webkit-border-radius: 0;
        -webkit-background-clip: padding-box;
        -moz-border-radius: 0;
        -moz-background-clip: padding;
        border-radius: 0;
        background-clip: padding-box;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        margin: 0;
        vertical-align: top;
        display: inline-block;
        width: 100%;
        font-size: 17px;
        color: #8d8d8d;
        background-color: #ffffff;
        border-style: solid;
        border-width: 1px 1px 1px 3px;
        border-top-color: #d7d7d7;
        border-right-color: #d7d7d7;
        border-bottom-color: #d7d7d7;
        outline: none;
    }

    input[type=text]:hover,
    input[type=email]:hover,
    input[type=password]:hover {
        color: #000000;
    }

    input[type=text]:focus,
    input[type=email]:focus,
    input[type=password]:focus {
        outline: none;
        border-left-color: steelblue;
    }

    .well h3 {
        text-shadow: -1px -1px #FFF, -2px -2px #FFF, -1px 1px #FFF, -2px 2px #FFF, 1px 1px #FFF, 2px 2px #FFF, 1px -1px #FFF, 2px -2px #FFF, -3px -3px 2px #BBB, -3px 3px 2px #BBB, 3px 3px 2px #BBB, 3px -3px 2px #BBB;
        color: steelblue;
        transition: all 1s;
    }
</style>
<nav class="navbar navbar-expand-lg navbar-default bg-light">
    <a class="navbar-brand" href="#">Watch Togeather</a>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 well">
            <c:if test="${not empty requestScope.msg}">
            <p class="text-center">
            <div class="alert alert-danger" role="alert">
                ${msg}</p>
        </div>
        </c:if>
        <h3 class="text-center">Sign Up</h3>
        <form accept-charset="UTF-8" class="form" method="post" action="/WatchTogeather_war_exploded/sign_up">
            <div class="col-xs-12">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Введите Имя" name="fname"/>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Введите Фамилию" name="lname"/>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Ваш E-mail" name="login" id="email"/>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <input type="password" class="form-control" name="password" id="password1"/>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <input type="password" class="form-control" name="rpassword" id="password2"/>
                </div>
            </div>
            <div class="text-center col-xs-12">
                <input type="submit" class="btn btn-default" value="Submit" onclick="validateForm()"/>
            </div>
        </form>
        <script>
            function validateForm() {
                // проверяем пароли
                // выбираем элементы
                var password1 = document.getElementById('password1');
                var password2 = document.getElementById('password2');
                // сравниваем написанное, если не равно, то:
                if (password1.value !== password2.value) {
                    // сообщаем пользователю, можно сделать как угодно
                    alert('Проверьте пароли!');
                    // сообщаем браузеру, что не надо обрабатывать нажатие кнопки
                    // как обычно, т. е. не надо отправлять форму
                    return false;
                }
                var password_regex = /[0-9a-zA-Z]*[A-Z]+[0-9a-zА-Я]*/i;
                if (!(password1.value.length >= 8 && password_regex.test(password1.value))) {
                    alert('Слабый пароль')
                }
                // проверяем email
                var email = document.getElementById('email');
                // регулярка для проверки, не знаю, работает ли приведенный в примере
                var email_regexp = /[0-9a-zа-я_A-ZА-Я]+@[0-9a-zа-я_A-ZА-Я^.]+\.[a-zа-яА-ЯA-Z]{2,4}/i;
                // проверяем значение поля email, если нет, то:
                if (!email_regexp.test(email.value)) {
                    alert('Проверьте email');
                    return false;
                }
            }
        </script>
    </div>
</div>
</body>

</html>