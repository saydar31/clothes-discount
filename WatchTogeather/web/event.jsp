<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aydar
  Date: 27.10.2019
  Time: 21:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Watch Together ${event.name}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-default bg-light">
    <a class="navbar-brand" href="#">Watch Togeather</a>
    <ul class="navbar-nav">
        <li class="active">
            <a class="nav-link" href="/WatchTogeather_war_exploded/">Главная </a>
        </li>
        <c:if test="${not empty sessionScope.user}">
            <li class="active">
                <a class="nav-link" href="/WatchTogeather_war_exploded/profile">Мoя страница</a>
            </li>
            <li class="active">
                <a class="nav-link" href="/WatchTogeather_war_exploded/im">Cooбщения</a>
            </li>
        </c:if>
        <c:if test="${empty sessionScope.user}">
            <li class="active"><a class="nav-link" href="/WatchTogeather_war_exploded/sign_in">Sign in</a></li>
            <li class="active"><a class="nav-link" href="/WatchTogeather_war_exploded/sign_up">Sign up</a></li>
        </c:if>
    </ul>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <%--            <img src="https://img07.rl0.ru/afisha/e280x160p0x0f551x315q85i/s.afisha.ru/mediastorage/c9/79/b0ac314048a14ad3a832a78779c9.jpg"--%>
            <%--                 class="rounded float-left" width="200" height="200"><br>--%>
        </div>
        <div class="col-md-8">
            <p><h6>${event.name}</h6>${event.place}<br>${event.date}</p>
        </div>
    </div>
</div>
<c:if test="${ not eventVisitors.contains(sessionScope.user)}">
    <form method="post" action="/WatchTogeather_war_exploded/event?event_id=<c:out value="${event.id}"></c:out>">
        <input type="submit" class="bullet-button" value="Хочу пойти">

    </form>
</c:if>
<h3>Спектакль хотят посетить:</h3>
<ul>
    <c:forEach items="${eventVisitors}" var="visitor">
        <li class="list-group-item"><a
                href="/WatchTogeather_war_exploded/profile?id=<c:out value="${visitor.id}"></c:out>">${visitor.firstName} ${visitor.lastName}</a>
        </li>
    </c:forEach>
</ul>
</body>
</html>
