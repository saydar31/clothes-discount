<%--
  Created by IntelliJ IDEA.
  User: aydar
  Date: 10.11.2019
  Time: 14:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${sessionScope.user.firstName} ${sessionScope.user.lastName} : Settings</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
</head>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-default bg-light">
    <a class="navbar-brand" href="#">Watch Together</a>
    <ul class="navbar-nav">
        <li class="active">
            <a class="nav-link" href="/WatchTogeather_war_exploded">Главная </a>
        </li>
        <li>
            <a class="nav-link" href="/WatchTogeather_war_exploded/profile">Мoя страница</a>
        </li>
        <li>
            <a class="nav-link" href="/WatchTogeather_war_exploded/im">Cooбщения</a>
        </li>
    </ul>
</nav>
<form action="/WatchTogeather_war_exploded/profile_settings" method="post" enctype="multipart/form-data">
    <label>First name<input type="text" name="newFirstName" value="${sessionScope.user.firstName}"></label><br>
    <label>Last name<input type="text" name="newLastName" value="${sessionScope.user.lastName}"></label><br>
    <label>Photo<input type="file" name="photo"></label><br>
    <label>Info <input type="text" name="info"></label><br>
    <input type="submit" value="Save">
</form>
</body>
</html>
