<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
Created by IntelliJ IDEA.
User: aydar
Date: 27.10.2019
Time: 18:46
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Watch Together: ${currentPageUser.firstName} ${currentPageUser.lastName}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <meta charset="UTF-16">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-default bg-light">
    <a class="navbar-brand" href="#">Watch Together</a>
    <ul class="navbar-nav">
        <li class="active">
            <a class="nav-link" href="/WatchTogeather_war_exploded">Главная </a>
        </li>
        <li>
            <a class="nav-link" href="/WatchTogeather_war_exploded/profile">Мoя страница</a>
        </li>
        <li>
            <a class="nav-link" href="/WatchTogeather_war_exploded/im">Cooбщения</a>
        </li>
    </ul>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <img src="<c:out value="${currentPageUser.avatarUrl}"></c:out>" class="rounded float-left" width="200" height="200">
            <br>
            <c:if test="${notMyPage}">
                <a href="/WatchTogeather_war_exploded/chat?id=<c:out value="${currentPageUser.id}"></c:out>"><button type="button" class="btn btn-primary btn-lg">Start chat</button></a>
            </c:if>
        </div>
        <div class="col-md-8">
            <p>${currentPageUser.firstName} ${currentPageUser.lastName}</p>
            <c:if test="${infoIsPresent}">
                ${currentPageUser.info}
            </c:if>
        </div>
    </div>
</div>
<c:if test="${not empty requestScope.eventList}">
    <h3>${currentPageUser.firstName} ${currentPageUser.lastName} собирается посетить</h3>
    <ul class="list-group">
        <c:forEach items="${eventList}" var="event">
            <li class="list-group-item"><a
                    href="<c:out value="/WatchTogeather_war_exploded/event?id=${event.id}"></c:out>"><c:out
                    value="${event.name}"></c:out></a></li>
        </c:forEach>
    </ul>
</c:if>
<c:if test="${myPage}">
    <a href="/WatchTogeather_war_exploded/logout">
        <button type="button" class="btn btn-primary btn-lg" formaction="/logout" formmethod="get">logout</button>
    </a>
    <a href="/WatchTogeather_war_exploded/profile_settings">
    <button type="button" class="btn btn-primary btn-lg" formaction="/logout" formmethod="get">settings</button>
</a>
</c:if>
</body>

</html>