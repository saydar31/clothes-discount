<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aydar
  Date: 30.10.2019
  Time: 21:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Messages</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-default bg-light">
    <a class="navbar-brand" href="#">Watch Together</a>
    <ul class="navbar-nav">
        <li class="active">
            <a class="nav-link" href="/WatchTogeather_war_exploded">Главная </a>
        </li>
        <li>
            <a class="nav-link" href="/WatchTogeather_war_exploded/profile">Мoя страница</a>
        </li>
        <li>
            <a class="nav-link" href="/WatchTogeather_war_exploded/im">Cooбщения</a>
        </li>
    </ul>
</nav>
<div class="card grey lighten-3 chat-room">
    <div class="card-body">

        <h3>${interlocutorName}</h3>
        <div class="row px-lg-2 px-2">

            <div class="col-md-6 col-xl-8 pl-md-3 px-lg-auto px-0">

                <div class="chat-message">

                    <ul class="list-unstyled chat">
                        <c:forEach items="${messages}" var="message">
                            <li class="d-flex justify-content-between mb-4">
                                <img src="${message.sender.avatarUrl}" alt="avatar"
                                     class="avatar rounded-circle mr-2 ml-lg-3 ml-0 z-depth-1">
                                <div class="chat-body white p-3 ml-2 z-depth-1">
                                    <div class="header">
                                        <strong class="primary-font">${message.sender.firstName}</strong>
                                    </div>
                                    <hr class="w-100">
                                    <p class="mb-0">
                                            ${message.value}
                                    </p>
                                </div>
                            </li>
                        </c:forEach>
                        <li class="white">
                            <form action="/WatchTogeather_war_exploded/chat?sender_id=<c:out value="${sessionScope.user.id}"></c:out>&recipient_id=<c:out value="${recipientId}"></c:out>"
                                  method="post">
                                <div class="form-group basic-textarea">
                                    <label for="message">Message</label>
                                    <textarea name="message" class="form-control pl-2 my-0" id="message"
                                              rows="3"
                                              placeholder="Type your message here..."></textarea>
                                </div>

                                <input type="submit"
                                       value="Send"
                                       class="btn btn-info btn-rounded btn-sm waves-effect waves-light float-right">
                            </form>
                        </li>

                    </ul>

                </div>

            </div>

        </div>
    </div>
</div>
<script>
    function sendMessage() {
        var message = document.getElementById("message").value;
        var url = "/WatchTogeather_war_exploded/chat";
        var request = new XMLHttpRequest();
        request.request.open("POST", url, true);
        request.send();
    }
</script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
