<%--
  Created by IntelliJ IDEA.
  User: aydar
  Date: 10.10.2019
  Time: 15:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Watch Together</title>
  </head>
  <body>
  <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <c:if test = "${not empty requestScope.users}">
    <c:forEach items="${requestScope.users}" var="user">
      <li>
        <c:out value="${user.info}"></c:out>
      </li>
    </c:forEach>
  </c:if>
  </body>
</html>
