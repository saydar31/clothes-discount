<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aydar
  Date: 30.10.2019
  Time: 20:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Chats</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-default bg-light">
    <a class="navbar-brand" href="#">Watch Together</a>
    <ul class="navbar-nav">
        <li class="active">
            <a class="nav-link" href="/WatchTogeather_war_exploded">Главная </a>
        </li>
        <li>
            <a class="nav-link" href="/WatchTogeather_war_exploded/profile">Мoя страница</a>
        </li>
        <li>
            <a class="nav-link" href="/WatchTogeather_war_exploded/im">Cooбщения</a>
        </li>
    </ul>
</nav>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6 col-xl-4 px-0">

            <h6 class="font-weight-bold mb-3 text-center text-lg-left">Member</h6>
            <div class="white z-depth-1 px-3 pt-3 pb-0">
                <ul class="list-unstyled friend-list">

                    <c:forEach items="${chatmates}" var="chatmate">
                        <li class="p-2">
                            <a href="/WatchTogeather_war_exploded/chat?id=<c:out value="${chatmate.id}"></c:out>"
                               class="d-flex justify-content-between">
                                <img src="${chatmate.avatarUrl}"
                                     alt="avatar"
                                     class="avatar rounded-circle d-flex align-self-center mr-2 z-depth-1" width="60"
                                     lang="60">
                                <div class="text-small">
                                    <strong>${chatmate.firstName} ${chatmate.lastName}</strong>
                                </div>
                            </a>
                        </li>
                    </c:forEach>
                </ul>
            </div>

        </div>
    </div>
</div>
</body>
</html>
