<%--
  Created by IntelliJ IDEA.
  User: aydar
  Date: 10.10.2019
  Time: 15:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

<head>

    <title>Watch Togeather</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>

</head>

<body>
<nav class="navbar navbar-expand-lg navbar-default bg-light">
    <a class="navbar-brand" href="#">Watch Togeather</a>
    <ul class="navbar-nav">
        <li class="active">
            <a class="nav-link" href="/WatchTogeather_war_exploded/">Главная </a>
        </li>
        <c:if test="${not empty sessionScope.user}">
            <li  class="active">
                <a class="nav-link" href="/WatchTogeather_war_exploded/profile">Мoя страница</a>
            </li>
            <li  class="active">
                <a class="nav-link" href="/WatchTogeather_war_exploded/im">Cooбщения</a>
            </li>
        </c:if>
        <c:if test="${empty sessionScope.user}">
            <li  class="active"><a class="nav-link" href="/WatchTogeather_war_exploded/sign_in">Sign in</a></li>
            <li  class="active"><a class="nav-link" href="/WatchTogeather_war_exploded/sign_up">Sign up</a></li>
        </c:if>
    </ul>
</nav>
<h3>Cпектакли</h3>
<c:if test="${not empty requestScope.events}">
    <ul class="list-group">
        <c:forEach items="${requestScope.events}" var="event">
            <li class="list-group-item">
                <a href="/WatchTogeather_war_exploded/event?id=<c:out value="${event.id}"></c:out>">
                    <div class="media">
                        <div class="media-body">
                            <h5 class="mt-0"><c:out value="${event.name}"></c:out></h5>
                            <p>Where: <c:out value="${event.place}"></c:out></p>
                            <p>When: <c:out value="${event.date}"></c:out></p>
                        </div>
                    </div>
                </a>
            </li>
        </c:forEach>
    </ul>
</c:if>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>