package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionKeeperObject {
    private static ConnectionKeeperObject ourInstance = new ConnectionKeeperObject();

    public static ConnectionKeeperObject getInstance() {
        return ourInstance;
    }
    private Connection connection;
    private ConnectionKeeperObject() {
        String username = "postgres";
        String password = "qwerty007";
        String url = "jdbc:postgresql://localhost:5432/postgres";

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new IllegalArgumentException();
        }
    }

    public Connection getConnection(){
        return connection;
    }

}
