package dao;

import java.util.Optional;

public interface Crud<T> {
    public void create(T t);

    public void update(T t);

    public Optional<T> find(long id);

    public void delete(long id);
}