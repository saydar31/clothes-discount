package servlets;

import models.SignInData;
import models.User;
import dao.SignInCrudImpl;
import dao.UserCrudImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/sign_up")
public class SignUpServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        if (user == null) {
            String path = "/sign_up.jsp";
            ServletContext servletContext = getServletContext();
            RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(path);
            requestDispatcher.forward(req, resp);
        } else {
            resp.sendRedirect("/WatchTogeather_war_exploded/profile");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fname = req.getParameter("fname");
        String lname = req.getParameter("lname");
        String password = req.getParameter("password");
        String rpassword = req.getParameter("rpassword");
        String login = req.getParameter("login");
        if (!SignInCrudImpl.getInstance().exist(login)) {
            UserCrudImpl userCrud = UserCrudImpl.getInstance();
            User user = new User(fname, lname, "");
            userCrud.create(user);
            SignInCrudImpl signInCrud = SignInCrudImpl.getInstance();
            signInCrud.create(new SignInData(login, password, user.getId()));
            req.getSession().setAttribute("user", user);
            resp.sendRedirect("/WatchTogeather_war_exploded/user");
        } else {
            req.setAttribute("msg", "Such email is already exist");
            getServletContext().getRequestDispatcher("/sign_up.jsp").forward(req, resp);
        }

    }
}
