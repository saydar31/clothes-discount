package servlets;

import models.User;
import dao.SignInCrudImpl;
import dao.UserCrudImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/sign_in")
public class SignInServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        String passwordCookieName = "password";
        String loginCookieName = "login";
        Cookie loginCookie = null;
        Cookie passwordCookie = null;
        if (cookies != null) {
            System.out.println("cookie");
            for (Cookie c : cookies) {
                if (c.getName().equals(passwordCookieName)) {
                    passwordCookie = c;
                }
                if (c.getName().equals(loginCookieName)) {
                    loginCookie = c;
                }
            }
        }

        if (loginCookie != null && passwordCookie != null) {
            System.out.println("cookie not null");
            SignInCrudImpl signInCrud = SignInCrudImpl.getInstance();
            String login = loginCookie.getValue();
            String password = passwordCookie.getValue();
            Long id = signInCrud.id(login, password);
            HttpSession session = req.getSession();
            session.setAttribute("user", UserCrudImpl.getInstance().find(id).get());
        } else {
            HttpSession session = req.getSession();
            User user = (User) session.getAttribute("user");
            if (user == null) {
                System.out.println("ok");
                String path = "/sign_in.jsp";
                ServletContext servletContext = getServletContext();
                RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(path);
                requestDispatcher.forward(req, resp);
            } else {
                resp.sendRedirect("/WatchTogeather_war_exploded/profile");
            }
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SignInCrudImpl signInCrud = SignInCrudImpl.getInstance();
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        System.out.println(login);
        if (!signInCrud.exist(login)) {
            System.out.println("loginde");
            req.setAttribute("msg", "User with login " + login + " dosen't exists");
            getServletContext().getRequestDispatcher("/sign_in.jsp").forward(req, resp);
        } else {
            Long id = signInCrud.id(login, password);
            if (id != null) {
                if (req.getAttribute("remember_me") != null) {
                    if ((Boolean) req.getAttribute("remember_me")) {
                        Cookie loginCookie = new Cookie("login", login);
                        int cookieMaxAge = 7 * 24 * 60 * 60 * 1000;
                        loginCookie.setMaxAge(cookieMaxAge);
                        resp.addCookie(loginCookie);
                        Cookie passwordCookie = new Cookie("password", password);
                        passwordCookie.setMaxAge(cookieMaxAge);
                        resp.addCookie(passwordCookie);
                    }

                }
                HttpSession session = req.getSession();
                session.setAttribute("user", UserCrudImpl.getInstance().find(id).get());
                resp.sendRedirect("/WatchTogeather_war_exploded/profile");
            } else {
                req.setAttribute("msg", "wrong password");
                req.setAttribute("correctLogin", login);
                getServletContext().getRequestDispatcher("/sign_in.jsp").forward(req, resp);
            }
        }

    }
}
